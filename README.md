# Kuber-nginx

## Content

- [Project Structure](#project-structure)

- [Getting started](#getting-started)

- [How to reach the web server?](#how-to-reach-the-web-server)

- [Very helpful resources used](#very-helpful-resources-used)

## Project structure 

```
kuber-nginx
├─ configmap.yaml   # application configuration 
├─ deployment.yaml  # deployment configuration
├─ ingress.yaml     # ingress configuration
├─ service.yaml     # service configuration
├─index.html        # service html page
└─html_page.png     # rendered html page
```

## Getting started

To replicate the result follow the speps. 

1. Start the minikube: 

```
minikube start
```
2. Enable ingress controller: 

```
minikube addons enable ingress
```

3. Apply configmap.yaml to update application configuration data: 

```
kubectl apply -f configmap.yaml
```

4. Apply deployment.yaml to create our deployment: 

```
kubectl apply -f deployment.yaml
```

5. Now, since we need to expose the deployment apply service.yaml: 

```
kubectl apply -f service.yaml
```

6. Finally we can apply ingress.yaml and create a tunnel: 

```
kubectl apply -f ingress.yaml
```

```
minikube tunnel
```

## How to reach the web server? 

1. From terminal we can reach the server using  curl: 

```
curl -H "Host: nginx-service.info" http://localhost
```

2. If we want to reach the web page we need to ensure that our browser sends the Host header in the request. (I work on mac) To reach this we can modify our hosts file (sudo nano /etc/hosts) just by adding extra line: ```127.0.0.1   nginx-service.info```. Now the web page is reachable by ```nginx-service.info```. The resulted html page: 

<img src="html_page.png" alt="Html rendered page" style="height: 250px; width:500;"/>


## Very helpful resources used: 

[Create and Access a Custom Web Page With Kubernetes](https://blog.devops.dev/create-and-access-a-custom-web-page-with-kubernetes-e7cbaf4588df)

[Docker & Kubernetes: Setting Up Ingress with Nginx Controller on Minikube (Mac)](https://www.bogotobogo.com/DevOps/Docker/Docker_Kubernetes_Nginx_Ingress_Controller_2.php)
